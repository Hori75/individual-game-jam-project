extends MarginContainer

func _ready():
	MusicSystem.play_main_menu()
	Input.set_custom_mouse_cursor(null)


func _on_NewGameButton_pressed():
	MusicSystem.play_gui_sfx()
	GameSystem.new_game()
	get_tree().change_scene("res://Scenes/New_Game.tscn")


func _on_CreditsButton_pressed():
	MusicSystem.play_gui_sfx()
	get_tree().change_scene("res://Scenes/Credits.tscn")


func _on_ExitButton_pressed():
	get_tree().quit()
