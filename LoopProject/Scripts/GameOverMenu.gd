extends MarginContainer

onready var wave_label = get_node("VBoxContainer/VBoxContainer/VBoxContainer/WaveLabel")
onready var score_label = get_node("VBoxContainer/VBoxContainer/VBoxContainer/ScoreLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	if GameSystem.wave < 10:
		wave_label.text = "You survived " + str(GameSystem.wave) + " waves"
	elif GameSystem.wave == 10 and GameSystem.escaped:
		wave_label.text = "You survived " + str(GameSystem.wave) + " waves\nand nearly escaped"
	else:
		wave_label.text = "You survived " + str(GameSystem.wave) + " waves\nbut were overwhelemed"
	score_label.text = "Score: " + str(GameSystem.score)
	Input.set_custom_mouse_cursor(null)

func _on_MainMenuButton_pressed():
	MusicSystem.play_gui_sfx()
	get_tree().change_scene("res://Scenes/Main_Menu.tscn")
