extends KinematicBody2D

export(int) var velocity = 500
export(int) var damage = 1
var direction;

func set_direction(angle):
	self.rotation = angle + 0.5 * PI
	direction = Vector2(cos(angle), sin(angle))
	
func get_damage():
	return damage

func _physics_process(delta):
	move_and_collide(direction * velocity * delta)

func _on_Timer_timeout():
	self.queue_free()

func _on_Area2D_area_entered(area):
	if area.is_in_group("enemies"):
		$Timer.stop()
		self.queue_free()
