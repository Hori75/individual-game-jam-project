extends Node2D

onready var turn_speed = deg2rad(6)
onready var player_bullet = preload("res://Scenes/PlayerBullet.tscn")
onready var main_controller = get_node("/root/Main/MainController")

var fire_salvo = 3
var time_per_salvo = 0.3
var fire_cooldown = 2

var alarmed = false
var current_rotate = 1
var cooldown_time = 0
var salvo_cooldown = 0
var salvo_left = 0

func determine_target():
	var enemies = get_tree().get_nodes_in_group("enemies")
	var shortest = Vector2.INF
	var nearest = null
	for enemy in enemies:
		var distance = enemy.global_position - self.global_position
		if (distance) < shortest:
			shortest = distance
			nearest = enemy
	return nearest

func shoot_bullet():
	var shot = player_bullet.instance()
	var shot2 = player_bullet.instance()
	shot.position = $Bullet_spawner.global_position
	shot2.position = $Bullet_spawner2.global_position
	shot.set_direction(self.rotation)
	shot2.set_direction(self.rotation)
	main_controller.add_child(shot)
	main_controller.add_child(shot2)
	$AudioStreamPlayer2D.play()

func _process(delta):
	if alarmed:
		if salvo_left > 0:
			salvo_cooldown -= delta
			cooldown_time = fire_cooldown
			if salvo_cooldown <= 0:
				salvo_cooldown = time_per_salvo
				salvo_left -= 1
				shoot_bullet()
		else:
			cooldown_time -= delta
			salvo_cooldown = 0
			if cooldown_time < 0:
				salvo_left = fire_salvo

func _physics_process(delta):
	if alarmed:
		var enemy = determine_target()
		if enemy != null:
			var dir = self.get_angle_to(enemy.global_position)
			if abs(dir)<turn_speed: #to close for full turn_speed
				self.rotation += dir #this is just a look_at
			else:
				if dir>0: 
					self.rotation += turn_speed #clockwise
				if dir<0: 
					self.rotation -= turn_speed #anit - clockwise


func _on_Range_area_entered(area):
	if area.is_in_group("enemies"):
		alarmed = true
