extends KinematicBody2D

export(int) var health = 5
export(float) var max_speed = 100.0
export(float) var turn_speed = 0.1
export(float) var damaged_view_time = 0.05
export(float) var fire_cooldown_time = 2
export(float) var enemy_fire_range = 500
export(float) var enemy_range = 200
export(int) var num_rays = 8

onready var enemy_bullet = preload("res://Scenes/EnemyBullet.tscn")

var damaged_view_duration = 0
var cooldown_duration = 0

var ray_directions = []
var interest = []
var danger = []

var velocity = Vector2.ZERO

func shoot_bullet():
	var shot = enemy_bullet.instance()
	shot.position = $FireSlot.global_position
	shot.set_direction(self.rotation)
	get_parent().add_child(shot)
	$AudioStreamPlayer2D.play()

func _ready():
	interest.resize(num_rays)
	danger.resize(num_rays)
	ray_directions.resize(num_rays)
	for i in num_rays:
		var angle = i * 2 * PI / num_rays
		ray_directions[i] = Vector2.RIGHT.rotated(angle)

func _process(delta):
	if damaged_view_duration > 0:
		damaged_view_duration -= delta
		if damaged_view_duration <= 0:
			$AnimatedSprite.animation = "default"
	if cooldown_duration > 0:
		cooldown_duration -= delta
	else:
		var target_of_interest = get_parent().get_interest_position(position)
		if position.distance_to(target_of_interest) < enemy_fire_range:
			shoot_bullet()
			cooldown_duration = fire_cooldown_time

func _physics_process(delta):
	var target_of_interest = get_parent().get_interest_position(position)
	velocity = Steering.follow(velocity, self.position, target_of_interest, max_speed)
	rotation = velocity.angle()
	if position.distance_to(target_of_interest) < enemy_range:
		velocity = Vector2.ZERO
	move_and_collide(velocity * delta)

func _on_Area2D_area_entered(area):
	if area.is_in_group("playersidebullets"):
		health -= area.get_parent().get_damage()
		$AnimatedSprite.animation = "damaged"
		damaged_view_duration = damaged_view_time
		if health <= 0:
			GameSystem.on_enemy_destroyed()
			self.queue_free()
