extends CanvasLayer

onready var title_label = get_node("VBoxContainer/VBoxContainer/TitleLabel")
onready var base_hp_label = get_node("VBoxContainer/VBoxContainer2/HBoxContainer/BaseHPLabel")
onready var scrap_label = get_node("VBoxContainer/VBoxContainer/ScrapLabel")
onready var repair_all_button = get_node("VBoxContainer/VBoxContainer2/HBoxContainer/RepairAllButton")
onready var repair_button = get_node("VBoxContainer/VBoxContainer2/HBoxContainer/RepairButton")
onready var turret_label = get_node("VBoxContainer/VBoxContainer2/HBoxContainer2/TurretLabel")
onready var add_turret_button = get_node("VBoxContainer/VBoxContainer2/HBoxContainer2/AddTurretButton")
onready var upgrade_turret_button = get_node("VBoxContainer/VBoxContainer2/HBoxContainer2/UpgradeTurretButton")
onready var escape_button = get_node("VBoxContainer/VBoxContainer2/HBoxContainer3/EscapeButton")

func _ready():
	title_label.text = "Wave " + str(GameSystem.wave) + " Report"
	if GameSystem.turret_level > 0:
		add_turret_button.queue_free()
	MusicSystem.play_wave_report()
	Input.set_custom_mouse_cursor(null)


func _process(delta):
	base_hp_label.text = "Base HP: " + str(GameSystem.base_life)
	scrap_label.text = "Scraps: " + str(GameSystem.scraps)
	turret_label.text = "Turret Lv. " + str(GameSystem.turret_level)
	repair_all_button.disabled = GameSystem.scraps < 2 or GameSystem.base_life == GameSystem.START_BASE_LIFE
	repair_button.disabled = GameSystem.scraps < 2 or GameSystem.base_life == GameSystem.START_BASE_LIFE
	if GameSystem.turret_level == 0:
		add_turret_button.disabled = GameSystem.scraps < 200 and GameSystem.turret_level == 0
		upgrade_turret_button.disabled = true
	else:
		upgrade_turret_button.disabled = GameSystem.scraps < 200 or GameSystem.turret_level >= 5
	escape_button.disabled = GameSystem.scraps < 1000


func _on_RepairAllButton_pressed():
	var to_be_repaired = GameSystem.START_BASE_LIFE - GameSystem.base_life
	var could_be_repaired = floor(GameSystem.scraps / 2)
	if could_be_repaired > to_be_repaired:
		MusicSystem.play_gui_sfx()
		GameSystem.base_life += to_be_repaired
		GameSystem.scraps -= to_be_repaired * 2
	else:
		MusicSystem.play_gui_sfx()
		GameSystem.base_life += could_be_repaired
		GameSystem.scraps -= could_be_repaired * 2


func _on_RepairButton_pressed():
	if GameSystem.scraps >= 2 and GameSystem.base_life < GameSystem.START_BASE_LIFE:
		MusicSystem.play_gui_sfx()
		GameSystem.scraps -= 2
		GameSystem.base_life += 1


func _on_AddTurretButton_pressed():
	if GameSystem.scraps >= 200:
		MusicSystem.play_gui_sfx()
		GameSystem.scraps -= 200
		GameSystem.turret_level += 1
		add_turret_button.queue_free()


func _on_UpgradeTurret_pressed():
	if GameSystem.scraps >= 200:
		MusicSystem.play_gui_sfx()
		GameSystem.scraps -= 200
		GameSystem.turret_level += 1


func _on_EscapeButton_pressed():
	if GameSystem.scraps >= 1000:
		MusicSystem.play_gui_sfx()
		GameSystem.scraps -= 1000
		GameSystem.escaped = true
		MusicSystem.play_win()
		get_tree().change_scene("res://Scenes/Win.tscn")


func _on_NextWaveButton_pressed():
	MusicSystem.play_gui_sfx()
	GameSystem.next_wave()
	if GameSystem.wave <= 10:
		get_tree().change_scene("res://Scenes/Main.tscn")
	else:
		GameSystem.wave -= 1
		MusicSystem.play_game_over()
		get_tree().change_scene("res://Scenes/Game_Over.tscn")
