extends Node2D

const MIN_X = 512.0
const MIN_Y = 300.0
const MAX_X = 2490.0
const MAX_Y = 1700.0

var camera_follow = true

func _process(delta):
	# Just follow player's position and restrict if it going outside
	if camera_follow:
		var player = get_node("/root/Main/MainController/Main_Plane")
		var next_position = Vector2.ZERO
		next_position.x = clamp(player.position.x, MIN_X, MAX_X)
		next_position.y = clamp(player.position.y, MIN_Y, MAX_Y)
		self.position = next_position
