extends Node2D

onready var enemy_plane = preload("res://Scenes/Small_Enemy.tscn")

func _on_Timer_timeout():
	if GameSystem.unspawned_enemy > 0:
		GameSystem.unspawned_enemy -= 1
		var randx = (randi() % 300) - 150
		var randy = (randi() % 300) - 150
		var rand_offset = Vector2(randx, randy)
		var enemy = enemy_plane.instance()
		enemy.position = position
		get_parent().add_child(enemy)
	else:
		$Timer.stop()
