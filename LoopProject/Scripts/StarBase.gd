extends StaticBody2D

export(float) var damaged_view_time = 0.05

signal base_destroyed()

var damaged_view_duration = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if damaged_view_duration > 0:
		damaged_view_duration -= delta
		if damaged_view_duration <= 0:
			$AnimatedSprite.animation = "default"

func _on_Area2D_area_entered(area):
	if area.is_in_group("enemysidebullets"):
		GameSystem.on_base_damaged()
		$AnimatedSprite.animation = "damaged"
		damaged_view_duration = damaged_view_time
		if GameSystem.base_life <= 0:
			emit_signal("base_destroyed")
