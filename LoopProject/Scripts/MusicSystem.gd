extends AudioStreamPlayer

func play_main_menu():
	self.stop()
	var bgm = load("res://Assets/Bgm/bensound-scifi.mp3")
	bgm.set_loop(true)
	self.volume_db = -10
	self.stream = bgm
	self.play()

func play_main_game():
	self.stop()
	var bgm = load("res://Assets/Bgm/foolboymedia__floating-in-the-midnight-breeze.mp3")
	bgm.set_loop(true)
	self.volume_db = -10
	self.stream = bgm
	self.play()
	
func play_wave_report():
	self.stop()
	var bgm = load("res://Assets/Bgm/adamcantwell__space-ambience.mp3")
	bgm.set_loop(true)
	self.volume_db = -10
	self.stream = bgm
	self.play()
	
func play_game_over():
	self.stop()
	var bgm = load("res://Assets/Bgm/the-toothpaste-vampires__ambient-space-noise.mp3")
	bgm.set_loop(true)
	self.volume_db = -10
	self.stream = bgm
	self.play()

func play_win():
	self.stop()
	var bgm = load("res://Assets/Bgm/speedenza__big-space-drone-8.mp3")
	bgm.set_loop(true)
	self.volume_db = -10
	self.stream = bgm
	self.play()

func play_gui_sfx():
	$"GUI sfx".play()
