extends KinematicBody2D

onready var turn_speed = deg2rad(6)
onready var player_bullet = preload("res://Scenes/PlayerBullet.tscn")

signal main_plane_stop()
signal main_plane_moving()
signal main_plane_destroyed()

export(int) var max_plane_speed = 200
export(int) var plane_increase = 100
export(int) var resistance = 100
export(float) var fire_cooldown = 0.25
export(float) var damaged_view_time = 0.05

var stop = false
var shooting = false
var speed = 0
var velocity = Vector2()
var cooldown_time = 0
var damaged_view_duration = 0

func start_shooting():
	shooting = true

func stop_shooting():
	shooting = false

func shoot_bullet():
	var shot = player_bullet.instance()
	var shot2 = player_bullet.instance()
	shot.position = $FireSlot1.global_position
	shot2.position = $FireSlot2.global_position
	shot.set_direction(self.rotation)
	shot2.set_direction(self.rotation)
	get_parent().add_child(shot)
	get_parent().add_child(shot2)
	$AudioStreamPlayer2D.play()

func _ready():
	self.connect("main_plane_stop", get_parent(), "_on_Main_Plane_main_plane_stop")
	self.connect("main_plane_moving", get_parent(), "_on_Main_Plane_main_plane_moving")
	self.connect("main_plane_destroyed", get_parent(), "_on_Main_Plane_main_plane_destroyed")

func _input(event):
	if event.is_action_pressed("fire"):
		start_shooting()
	if event.is_action_released("fire"):
		stop_shooting()

func _process(delta):
	if shooting and cooldown_time <= 0:
		shoot_bullet()
		cooldown_time = fire_cooldown
	cooldown_time -= delta
	if damaged_view_duration > 0:
		damaged_view_duration -= delta
		if damaged_view_duration <= 0:
			$AnimatedSprite.animation = "default"
	
func _physics_process(delta):
	var dir = self.get_angle_to(get_global_mouse_position())
	if abs(dir)<turn_speed: #to close for full turn_speed
		self.rotation += dir #this is just a look_at
	else:
		if dir>0: 
			self.rotation += turn_speed #clockwise
		if dir<0: 
			self.rotation -= turn_speed #anit - clockwise
	if stop:
		speed -= resistance * delta
		if speed < 0:
			speed = 0
	else:
		speed += plane_increase * delta
		if speed > max_plane_speed:
			speed = max_plane_speed
	velocity.x = speed
	velocity = position.direction_to(get_global_mouse_position()) * speed
	velocity = move_and_slide(velocity)

func _on_SpeedControl_mouse_entered():
	emit_signal("main_plane_stop")
	stop = true
	$AnimatedSprite.animation = "default"

func _on_SpeedControl_mouse_exited():
	emit_signal("main_plane_moving")
	stop = false
	$AnimatedSprite.animation = "moving"

func _on_PlaneArea_area_entered(area):
	if area.is_in_group("enemysidebullets"):
		GameSystem.on_plane_damaged()
		$AnimatedSprite.animation = "damaged"
		damaged_view_duration = damaged_view_time
		if GameSystem.plane_life <= 0:
			GameSystem.on_player_destroyed()
			emit_signal('main_plane_destroyed')
			self.queue_free()
