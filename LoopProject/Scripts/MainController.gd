extends Node2D

onready var cursor_1 = load("res://Assets/Cursor/cursor_1.png")
onready var cursor_2 = load("res://Assets/Cursor/cursor_2.png")
onready var loaded_turret = preload("res://Scenes/Turret.tscn")
onready var loaded_plane = preload("res://Scenes/Main_Plane.tscn")

signal wave_cleared()

var player_dead = false
var cleared = false

func get_interest_position(position):
	if not player_dead:
		var main_plane_pos = $Main_Plane.global_position
		var star_base_pos = $StarBase.global_position
		if position.distance_to(main_plane_pos) < position.distance_to(star_base_pos):
			return main_plane_pos
		else:
			return star_base_pos
	else:
		return $StarBase.global_position


func respawn_player():
	var plane = loaded_plane.instance()
	plane.position = $PlayerSpawn.global_position
	self.add_child(plane)


func _ready():
	respawn_player()
	if GameSystem.turret_level > 0:
		var turret = loaded_turret.instance()
		turret.fire_salvo = GameSystem.turret_level * 2
		$StarBase.add_child(turret)
	MusicSystem.play_main_game()
	Input.set_custom_mouse_cursor(cursor_1,Input.CURSOR_ARROW, Vector2(16,16))


func _process(delta):
	update()
	if GameSystem.unspawned_enemy <= 0 and get_tree().get_nodes_in_group("enemies").size() == 0 and not cleared:
		cleared = true
		$ClearTimer.start()
		emit_signal("wave_cleared")


func _draw():
	if not player_dead:
		draw_line(get_global_mouse_position(), $Main_Plane.position, Color(255, 0, 0), 1)


func _on_Main_Plane_main_plane_moving():
	Input.set_custom_mouse_cursor(cursor_1,Input.CURSOR_ARROW, Vector2(16,16))


func _on_Main_Plane_main_plane_stop():
	Input.set_custom_mouse_cursor(cursor_2,Input.CURSOR_ARROW, Vector2(16,16))


func _on_Main_Plane_main_plane_destroyed():
	$CameraControl.camera_follow = false
	player_dead = true
	$RespawnTimer.start(5)


func _on_RespawnTimer_timeout():
	$RespawnTimer.stop()
	GameSystem.respawn_player()
	respawn_player()
	$CameraControl.camera_follow = true
	player_dead = false


func _on_StarBase_base_destroyed():
	MusicSystem.play_game_over()
	get_tree().change_scene("res://Scenes/Game_Over.tscn")


func _on_ClearTimer_timeout():
	get_tree().change_scene("res://Scenes/WaveReport.tscn")
