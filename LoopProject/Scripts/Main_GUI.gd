extends MarginContainer

onready var base_hp_bar = get_node("HBoxContainer/LifeBars/BaseHP/BaseHPBar")
onready var plane_hp_bar = get_node("HBoxContainer/LifeBars/PlaneHP/PlaneHPBar")
onready var wave_label = get_node("HBoxContainer/Score/WaveLabel")
onready var score_label = get_node("HBoxContainer/Score/ScoreLabel")
onready var scraps_label = get_node("HBoxContainer/Score/ScrapsLabel")
onready var respawn_time_label = get_node("HBoxContainer/LifeBars/RespawnTimer")
onready var respawn_timer = get_node("/root/Main/MainController/RespawnTimer")

func _ready():
	base_hp_bar.max_value = GameSystem.START_BASE_LIFE
	plane_hp_bar.value = GameSystem.START_PLANE_LIFE
	wave_label.text = "Wave " + str(GameSystem.wave)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	base_hp_bar.value = GameSystem.base_life
	plane_hp_bar.value = GameSystem.plane_life
	score_label.text = "Score: " + str(GameSystem.score)
	scraps_label.text = "Scraps: "+ str(GameSystem.scraps)
	if GameSystem.need_respawn:
		respawn_time_label.text = "Respawn: " + str(ceil(respawn_timer.get_time_left())) + " s"
	else:
		respawn_time_label.text = ""


func _on_MainController_wave_cleared():
	wave_label.text = "Wave " + str(GameSystem.wave) + " Cleared!"
