extends Node

const START_BASE_LIFE = 100
const START_PLANE_LIFE = 30
const START_ENEMY_WAVE = 0
const START_SCORE = 0
const START_SCRAPS = 0
const ENEMY_SCORE_YIELD = 100
const ENEMY_SCRAP_YIELD = 10

var need_respawn
var base_life
var plane_life
var enemy_wave
var unspawned_enemy
var score
var scraps
var wave
var turret_level
var escaped

func new_game():
	base_life = START_BASE_LIFE
	plane_life = START_PLANE_LIFE
	enemy_wave = START_ENEMY_WAVE
	unspawned_enemy = 0
	score = START_SCORE
	scraps = START_SCRAPS
	need_respawn = false
	escaped = false
	turret_level = 0
	wave = 0
	next_wave()

func on_plane_damaged():
	plane_life -= 1

func on_player_destroyed():
	need_respawn = true

func respawn_player():
	plane_life = START_PLANE_LIFE
	need_respawn = false

func on_base_damaged():
	base_life -= 1

func on_enemy_destroyed():
	score += ENEMY_SCORE_YIELD
	scraps += ENEMY_SCRAP_YIELD

func next_wave():
	enemy_wave += 5
	wave += 1
	unspawned_enemy += enemy_wave
	plane_life = START_PLANE_LIFE
